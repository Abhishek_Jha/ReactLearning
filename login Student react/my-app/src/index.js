import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { withAlert } from 'react-alert'

import "./Login.css";
import studentDetails from './studentDetails';
import viewStudentDetails from './viewStudentDetails';
import test from './test';

export default class Login extends Component {
  constructor(props) {
    super(props);


    this.state = {
      email: "",
      password: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateForm = this.validateForm.bind(this);
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = event => {
    event.preventDefault();
  }

  render() {
    return (
      <div className="Login">

     
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email" bsSize="large">
            <ControlLabel>Email</ControlLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          <Button
            block
            bsSize="large"
            disabled={!this.validateForm()}
            alert='LoggedIn Success...!'
            type="submit"
          >
            Login
          </Button>&nbsp;

       

      <Button bsStyle="success">SignUp</Button>
    
        </form>
      </div>
    );
  }
 
}

class App extends Component {
  render() {
     return (
        <Router>
           <div>
              <h2>Welcome to the example ...!</h2>
              <ul>
                 <li><Link to={'/'}>Login</Link></li>
                 <li><Link to={'/studentDetails'}>studentDetails</Link></li>
                 <li><Link to={'/viewStudentDetails'}>viewStudentDetails</Link></li>
                 <li><Link to={'/test'}>test</Link></li>
              </ul>
              <hr />
              
              <Switch>
                 <Route exact path='/' component={Login} />
                 <Route exact path='/studentDetails' component={studentDetails} />
                 <Route exact path='/viewStudentDetails' component={viewStudentDetails} />
                 <Route exact path='/test' component={test} />
              </Switch>
           </div> 
        </Router>
     );
    }
  } 

ReactDOM.render(
   <App />,
  // <Login/>,
  document.getElementById('root')
);
