import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
//var tableData;


export default class viewStudentDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
     tableData:[],
    };
    // this.callApi = this.callApi.bind(this);
    // this.handleChange = this.handleChange.bind(this);
    // this.handleSubmit = this.handleSubmit.bind(this);
    // this.validateForm = this.validateForm.bind(this);
  }


  componentDidMount() {
    
     this.callApi()
      .then(res => {
        this.setState({ response : res })
      })
      .catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch('http://localhost:8000/getStudent');
    const body = await response.json();
    console.log('bodyx  x =>',body);
    this.state.tableData = body;
    console.log('tableData=======>',this.state.tableData);

    if (response.status !== 200) throw Error(body.message);

    return body;
  };


  validateForm() {
    return this.state.name.length > 0 && this.state.age.length > 0 && this.state.PUC.length > 0 && this.state.graduation.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = event => {
    event.preventDefault();
  }

  render() {
    //const response = await fetch('http://localhost:8000/getStudent');
    //const body = await response.json();
    //const item = [{ "_id": "5b0e5008c1432b4d87b8b145", "email": "abhi.jh10@gmail.com", "Name": "1234", "PUC": "211", "graduation": "12", "__v": 0 }, { "_id": "5b0e502dc1432b4d87b8b146", "email": "abhi10.jh10@gmail.com", "Name": "Abhishek", "PUC": "100", "graduation": "27", "__v": 0 }, { "_id": "5b0e503fc1432b4d87b8b147", "email": "abhi1111.jh10@gmail.com", "Name": "Dinesh", "PUC": "90", "graduation": "24", "__v": 0 }];
var items = this.state.tableData;

console.log('this.state.tableData=====',this.state.tableData)
       items = items.map(function (item) {
        return (
          <tr>
            <td>{item._id}</td>
            <td>{item.email}</td>
            <td>{item.Name}</td>
           <td>{item.PUC}</td>
            <td>{item.graduation}</td>
            {/* <td>{item.English}</td> */}
            <td>{parseInt(item.graduation)+parseInt(item.PUC)} </td>
          </tr>    
        );
      });
      return (
        <div className="Student Details">
          <table>
            <tr>
            <th>_id</th>
              <th>email</th>
              <th>Student Name</th>
              <th>Math</th>
              <th>Science</th>
              {/* <th>English</th> */}
              <th>Total Marks   </th>
            </tr>
            {items}
              </table>
          <div>
  
          </div>
  
        </div>
      );
  }

}

ReactDOM.render(
  // <Game />,
  <studentDetails />,
  document.getElementById('root')
);
