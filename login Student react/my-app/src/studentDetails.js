import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";



//import React, { Component } from 'react';
    import axios from 'axios';

    export default  class studentDetails extends Component {
      constructor() {
        super();
        this.state = {
          email: '',
          Name: '',
          PUC: 0,
          graduation:0,
        };
      }

      onChange = (e) => {
        // Because we named the inputs to match their corresponding values in state, it's
        // super easy to update the state
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
      }

      onSubmit = (e) => {
        e.preventDefault();
        // get our form data out of state
        const { email, Name, PUC, graduation } = this.state;

        axios.post('/student', { email, Name, PUC, graduation })
          .then((result) => {
            //access the results here....
          });
      }

      render() {
        const { email, Name, PUC, graduation } = this.state;
        return (
          <form onSubmit={this.onSubmit}>
          <h1> Please Fill out all the Detail Asked by the form</h1>
            <label>
         Student email:
          
          </label>
            <input type="text" name="email" value={email} onChange={this.onChange} />
            <label>
         Student Name:
          
          </label>
            <input type="text" name="Name" value={Name} onChange={this.onChange} />
            <label>
          Math Marks:
          
          </label>
            <input type="text" name="PUC" value={PUC} onChange={this.onChange} />
            <label>
          Science Marks:
          
          </label>
            <input type="text" name="graduation" value={graduation} onChange={this.onChange} />
            <button type="submit">Submit</button>
          </form>
        );
      }
    }


// export default class studentDetails extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       email:'',
//       Name: '',
//       PUC:0,
//       graduation:0
     

//     };

//     this.handleChange = this.handleChange.bind(this);
//     this.handleSubmit = this.handleSubmit.bind(this);
//   }

//   componentDidMount() {
    
//     this.callApi()
//      .then(res => {
//        this.setState({ response : res })
//      })
//      .catch(err => console.log(err));
//  }

//  callApi = async () => {
//    const response = await fetch('http://localhost:8000/student');
//    const body = await response.json();
//    console.log('bodyx  x =>',body);
//    this.state.tableData = body;
//    console.log('tableData=======>',this.state.tableData);

//    if (response.status !== 200) throw Error(body.message);

//    return body;
//  };

//   handleChange(event) {
//     this.setState({Name: event.target.Name});
//     this.setState({email: event.target.email});
//     this.setState({PUC: event.target.PUC});
//     this.setState({graduation: event.target.graduation});
//   }

//   handleSubmit(event) {
//     alert('Student name was submitted: ' + this.state.Name);
//     event.preventDefault();
//   }

//   render() {
//     return (
//       <form onSubmit={this.handleSubmit}>
//         <label>
//           Student email:
//           <input type="email" value={this.state.email} onChange={this.handleChange} />
//         </label>
//         <label>
//           Student Name:
//           <input type="text" value={this.state.Name} onChange={this.handleChange} />
//         </label>
//         <label>
//           Math Mark:
//           <input type="text" value={this.state.PUC} onChange={this.handleChange} />
//         </label>
//         <label>
//           English Mark:
//           <input type="text" value={this.state.graduation} onChange={this.handleChange} />
//         </label>
//         <input type="submit" value="Submit" />
//       </form>
//     );
//   }
// }

// // ReactDOM.render(
// //   <studentDetails />,
// //   document.getElementById('root')
// // );

ReactDOM.render(
  // <Game />,
  <studentDetails/>,
  document.getElementById('root')
);
