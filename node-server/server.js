var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
var passport    = require('passport');

var jwt         = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config/config'); // get our config file
var student   = require('./model/studentModel'); // get our mongoose model
var api = require('./route/api');
var User = require('./model/user');
var pass = require('./config/passport');


// =======================
// configuration =========
// =======================
var port = process.env.PORT || 8000; // used to create, sign, and verify tokens
mongoose.connect(config.database); // connect to database
app.set('abcdefghijklmnopqurstuvwxyz', config.secret); // secret variable

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(passport.initialize())
app.use(passport.session())

// use morgan to log requests to the console
app.use(morgan('dev'));

// =======================
// routes ================
// =======================
// basic route
app.get('/', function(req, res) {
    res.send('Hello!  http://localhost:' + port + '');
});

//=============================================================================
app.post('/signup', function(req, res) {
    if (!req.body.username || !req.body.password) {
      res.json({success: false, msg: 'Please pass username and password.'});
    } else {
      var newUser = new User({
        username: req.body.username,
        password: req.body.password
      });
      // save the user
      newUser.save(function(err) {
        if (err) {
            console.log(err);
          return res.json({success: false, msg: 'Username already exists.'});
        }
        res.json({success: true, msg: 'Successful created new user.'});
      });
    }
  });
  //=============================================================================
  // User Login
  app.post('/login', function(req, res) {
    User.findOne({
      username: req.body.username
    }, function(err, user) {
      if (err) throw err;
  
      if (!user) {
        res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
      } else {
        // check if password matches
        user.comparePassword(req.body.password, function (err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            var token = jwt.sign(user, config.secret);
            // return the information including token as JSON
            res.json({success: true, token: 'JWT ' + token});
          } else {
            res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
          }
        });
      }
    });
  });

  //===================================================================================

//post stident details
app.post('/student', function(req, res) {
    //var token = getToken(req.headers);
    if (req.body) {
      console.log(req.body);
      var studentDet = new student({
        email: req.body.email,
        Name: req.body.Name,
        PUC: req.body.PUC,
        graduation: req.body.graduation,
        English: req.body.English
      });
  
      studentDet.save(function(err) {
        if (err) {
          return res.json({success: false, msg: 'student Details save failed.'});
        }
        res.json({success: true, msg: 'Successful created new student.'});
      });
    } else {
      return res.status(403).send({success: false, msg: 'Unauthorized.'});
    }
  });
  //================================================================================

  app.get('/getStudent',  function(req, res) {
   // var token = getToken(req.headers);
    if (req.body) {
      student.find(function (err, student) {
        if (err) return next(err);
        res.json(student);
      });
    } else {
      return res.status(403).send({success: false, msg: 'Unauthorized.'});
    }
  });

// API ROUTES -------------------
// we'll get to these in a second

// =======================
// start the server ======
// =======================
app.listen(port);
console.log('Server is running at http://localhost:' + port);

module.exports = app;