var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/config');
require('../config/passport')(passport);
var express = require('express');
var jwt = require('jsonwebtoken');
var app = express.Router();
var User = require("../model/user");
var student = require("../model/studentModel");


//module.exports = function (app, passport) {
//User SignUp

app.post('/signup', function(req, res) {
    console.log('helloooooooooooooooooooooo');
    if (!req.body.username || !req.body.password) {
      res.json({success: false, msg: 'Please pass username and password.'});
    } else {
        console.log('helloooooooooooooooooooooo');
      var newUser = new User({
        username: req.body.username,
        password: req.body.password
      });
      // save the user
      newUser.save(function(err) {
        if (err) {
          return res.json({success: false, msg: 'Username already exists.'});
        }
        res.json({success: true, msg: 'Successful created new user.'});
      });
    }
  });



 // User Login
  app.post('/login', function(req, res) {
    User.findOne({
      username: req.body.username
    }, function(err, user) {
      if (err) throw err;
  
      if (!user) {
        res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
      } else {
        // check if password matches
        user.comparePassword(req.body.password, function (err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            var token = jwt.sign(user, config.secret);
            // return the information including token as JSON
            res.json({success: true, token: 'JWT ' + token});
          } else {
            res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
          }
        });
      }
    });
  });

//post stident details
  app.post('/student', function(req, res) {
    //var token = getToken(req.headers);
    if (!err) {
      console.log(req.body);
      var studentDet = new student({
        email: req.body.email,
        Name: req.body.Name,
        PUC: req.body.PUC,
        graduation: req.body.graduation
      });
  
      newStudent.save(function(err) {
        if (err) {
          return res.json({success: false, msg: 'student Details save failed.'});
        }
        res.json({success: true, msg: 'Successful created new student.'});
      });
    } else {
      return res.status(403).send({success: false, msg: 'Unauthorized.'});
    }
  });



//get the list of the all the books

  app.get('/getdata', passport.authenticate('jwt', { session: false}), function(req, res) {
    var token = getToken(req.headers);
    if (token) {
      studentModel.find(function (err, student) {
        if (err) return next(err);
        res.json(student);
      });
    } else {
      return res.status(403).send({success: false, msg: 'Unauthorized.'});
    }
  });
//}