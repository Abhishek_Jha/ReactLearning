var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var studentSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  Name: {
    type: String,
    required: true
  },
  PUC: {
    type: String,
    required: true
  },
  graduation: {
    type: String,
    required: true
  }, English: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model('student', studentSchema);